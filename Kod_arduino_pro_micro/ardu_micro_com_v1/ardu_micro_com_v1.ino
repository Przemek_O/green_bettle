#include <SoftwareSerial.h>

#define Pin_Cell1 A0
#define Pin_Cell2 A1

#define motor1Pin1 2
#define motor1Pin2 3
#define motor2Pin1 4
#define motor2Pin2 5
#define led1Pin 16
#define led2Pin 14
#define led3Pin 15
#define buzzerPin 6

SoftwareSerial mySerial(9, 10); // RX, TX

int period = 1000;
unsigned long time_now = 0;
int i;
int FLAG;
int message;
int volt1=0;
int volt2=0;
const int buzzer = 6; //Define pin 10, can use other PWM pins  (5,6 or 9)
                      //Note pins 3 and 11 can't be used when using the tone function in Arduino Uno
const int songspeed = 1.5; //Change to 2 for a slower version of the song, the bigger the number the slower the song
//*****************************************
#define NOTE_C4  262   //Defining note frequency
#define NOTE_D4  294
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_G4  392
#define NOTE_A4  440
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_D5  587
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_G5  784
#define NOTE_A5  880
#define NOTE_B5  988
//*****************************************
int notes[] = {       //Note of the song, 0 is a rest/pulse
   NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, 
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
   NOTE_A4, NOTE_G4, NOTE_A4, 0,
   
   NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, 
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
   NOTE_A4, NOTE_G4, NOTE_A4, 0,
   
   NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, 
   NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0, 
   NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0,
   NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0,
   
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_D5, NOTE_E5, NOTE_A4, 0, 
   NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0,
   NOTE_C5, NOTE_A4, NOTE_B4, 0,

   NOTE_A4, NOTE_A4, 
   //Repeat of first part
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
   NOTE_A4, NOTE_G4, NOTE_A4, 0,

   NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, 
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_C5, NOTE_D5, NOTE_B4, NOTE_B4, 0,
   NOTE_A4, NOTE_G4, NOTE_A4, 0,
   
   NOTE_E4, NOTE_G4, NOTE_A4, NOTE_A4, 0, 
   NOTE_A4, NOTE_C5, NOTE_D5, NOTE_D5, 0, 
   NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, 0,
   NOTE_E5, NOTE_D5, NOTE_E5, NOTE_A4, 0,
   
   NOTE_A4, NOTE_B4, NOTE_C5, NOTE_C5, 0, 
   NOTE_D5, NOTE_E5, NOTE_A4, 0, 
   NOTE_A4, NOTE_C5, NOTE_B4, NOTE_B4, 0,
   NOTE_C5, NOTE_A4, NOTE_B4, 0,
   //End of Repeat

   NOTE_E5, 0, 0, NOTE_F5, 0, 0,
   NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0,
   NOTE_D5, 0, 0, NOTE_C5, 0, 0,
   NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4,

   NOTE_E5, 0, 0, NOTE_F5, 0, 0,
   NOTE_E5, NOTE_E5, 0, NOTE_G5, 0, NOTE_E5, NOTE_D5, 0, 0,
   NOTE_D5, 0, 0, NOTE_C5, 0, 0,
   NOTE_B4, NOTE_C5, 0, NOTE_B4, 0, NOTE_A4
};
//*****************************************
int duration[] = {         //duration of each note (in ms) Quarter Note is set to 250 ms
  125, 125, 250, 125, 125, 
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 375, 125, 
  
  125, 125, 250, 125, 125, 
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 375, 125, 
  
  125, 125, 250, 125, 125, 
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 125, 250, 125,

  125, 125, 250, 125, 125, 
  250, 125, 250, 125, 
  125, 125, 250, 125, 125,
  125, 125, 375, 375,

  250, 125,
  //Rpeat of First Part
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 375, 125, 
  
  125, 125, 250, 125, 125, 
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 375, 125, 
  
  125, 125, 250, 125, 125, 
  125, 125, 250, 125, 125,
  125, 125, 250, 125, 125,
  125, 125, 125, 250, 125,

  125, 125, 250, 125, 125, 
  250, 125, 250, 125, 
  125, 125, 250, 125, 125,
  125, 125, 375, 375,
  //End of Repeat
  
  250, 125, 375, 250, 125, 375,
  125, 125, 125, 125, 125, 125, 125, 125, 375,
  250, 125, 375, 250, 125, 375,
  125, 125, 125, 125, 125, 500,

  250, 125, 375, 250, 125, 375,
  125, 125, 125, 125, 125, 125, 125, 125, 375,
  250, 125, 375, 250, 125, 375,
  125, 125, 125, 125, 125, 500
};

//lipo_1
int return_voltage1 ( int Analog_Pin) {

  // zakres pracy lipoli- 3 do 4,2 V
  int actual_value = analogRead(Analog_Pin); 
//  Serial.print(actual_value);// value 0-1023 
//  Serial.print(" ");
  double actual_voltage = map(actual_value, 0 , 1023 , 0 , 500); // value 0-5 V
//  Serial.print(actual_voltage);
//  Serial.print(" ");
  actual_voltage=actual_voltage;
  //Serial.print(actual_voltage);
  //Serial.println("V");
  //int actual_percent = map(actual_voltage, 320 , 405 , 0 , 100); // value 0-100%,from 3,2 to 4,2 V
  int actual_percent = map(actual_voltage, 320 , 386 , 0 , 100); // value 0-100%,from 3,2 to 4,2 V

//  Serial.print(actual_percent);
//  Serial.println(" ");

  if ( Analog_Pin == "Pin_Cell1" ) {
    Serial.print("Voltage on cell 1: ");
    Serial.print(actual_voltage);
    Serial.println("V!");

  }
   
  return actual_percent;
}

//lipo_2
int return_voltage2 ( int Analog_Pin1, int Analog_Pin2) {

  // zakres pracy lipoli- 3 do 4,2 V
  int actual_value1 = analogRead(Analog_Pin1); 
//  Serial.print(actual_value);// value 0-1023 
//  Serial.print(" ");
  double actual_voltage1 = map(actual_value1, 0 , 1023 , 0 , 500); // value 0-5 V
//  Serial.print(actual_voltage);
//  Serial.print(" ");
  //actual_voltage=actual_voltage;
  int actual_value2 = analogRead(Analog_Pin2); 
//  Serial.print(actual_value);// value 0-1023 
//  Serial.print(" ");
  double actual_voltage2 = map(actual_value2, 0 , 1023 , 0 , 500); // value 0-5 V
//  Serial.print(actual_voltage);
//  Serial.print(" ");
  //actual_voltage=actual_voltage;
  
double R1=3300;
double R2=4700;
double actual_voltage_both_lipo = ((actual_voltage2 * (R1 + R2))/R2);
double actual_voltage_second_lipo = actual_voltage_both_lipo - actual_voltage1; 
  //Serial.print(actual_voltage);
  //Serial.println("V");
  int actual_percent = map(actual_voltage_second_lipo, 320 , 386 , 0 , 100); // value 0-100%,from 3,76 to 4,935 V
//  Serial.print(actual_percent);
//  Serial.println(" ");


  if ( Analog_Pin1 == "Pin_Cell1" ){
    Serial.print("Voltage on cell 2: ");
    Serial.print(actual_voltage_second_lipo);
    Serial.println("V!");
  }
  
  return actual_percent;
}

void stopMotors() {
  digitalWrite(motor1Pin1,LOW);
  digitalWrite(motor1Pin2,LOW);
  digitalWrite(motor2Pin1,LOW);
  digitalWrite(motor2Pin2,LOW);
  Serial.println("Both_motors_STOP!");
}

void goBack() {
  digitalWrite(motor1Pin1,LOW);
  digitalWrite(motor1Pin2,HIGH);
  digitalWrite(motor2Pin1,LOW);
  digitalWrite(motor2Pin2,HIGH);
  Serial.println("GO_BACK!");
}

void goStraight() {
  digitalWrite(motor1Pin1,HIGH);
  digitalWrite(motor1Pin2,LOW);
  digitalWrite(motor2Pin1,HIGH);
  digitalWrite(motor2Pin2,LOW);
  Serial.println("GO_STRAIGHT!");
}

void turnLeft() {
  digitalWrite(motor1Pin1,LOW);
  digitalWrite(motor1Pin2,HIGH);
  digitalWrite(motor2Pin1,HIGH);
  digitalWrite(motor2Pin2,LOW);
  Serial.println("TURN_LEFT!");
}

void turnRight() { 
  digitalWrite(motor1Pin1,HIGH);
  digitalWrite(motor1Pin2,LOW);
  digitalWrite(motor2Pin1,LOW);
  digitalWrite(motor2Pin2,HIGH);
  Serial.println("TURN_RIGHT!");
}

void setup()
{ 
  pinMode(led1Pin,OUTPUT);
  pinMode(led2Pin,OUTPUT);
  pinMode(led3Pin,OUTPUT);
  pinMode(buzzerPin,OUTPUT);
  
  
  pinMode(motor1Pin1,OUTPUT);
  pinMode(motor1Pin2,OUTPUT);
  pinMode(motor2Pin1,OUTPUT);
  pinMode(motor2Pin2,OUTPUT);

  Serial.begin(115200); 
  Serial.println("I'm Micro, I'm started!");
  Serial.setTimeout(50);

  mySerial.begin(9600);
  mySerial.println("I'm Micro, I'm started!");  
  mySerial.setTimeout(50);
FLAG=0;
i=1;

}

void loop() {
  volt1=return_voltage1(Pin_Cell1); // call read voltage function on the first battery
  volt2=return_voltage2(Pin_Cell1,Pin_Cell2); // call read voltage function on the second battery


if(mySerial.available()) {
  message=mySerial.parseInt();
  Serial.print("mySerial: ");
  Serial.println(message);
}      

if(Serial.available()) {
  message=Serial.parseInt();
  Serial.println("Serial: ");
  Serial.println(message);
}      
      
     if (message == 1 ) {
          goStraight();
          Serial.println("GO_STRAIGHT! - code:1");
        }
        
     if (message == 2 ) {
        stopMotors();
        Serial.println("STOP! - code:2");
      }
      
    if (message == 3 ) {
      goBack();
      Serial.println("GO_BACK! - code:3");
    }
    
    if (message == 4 ) {
      turnLeft();
      Serial.println("TURN_LEFT! - code:4");
    }
    
    if (message == 5 ) {
      turnRight();
      Serial.println("TURN_RIGHT! - code:5");
    }
    
    if (message == 6 ) {
      digitalWrite(led1Pin, HIGH);
      digitalWrite(led2Pin, HIGH);
      Serial.println("LED1_ON! - code:6");
    }
    
    if (message == 7 ) {
      digitalWrite(led1Pin, LOW);
      digitalWrite(led2Pin, LOW);
      Serial.println("LED1_OFF! - code:7");
    }
    
    if (message == 8 ) {
      digitalWrite(led3Pin, HIGH);
      Serial.println("LED2_ON! - code:8");
    }
    
    if (message == 9 ) {
      digitalWrite(led3Pin, LOW);
      Serial.println("LED2_OFF! - code:9");     
    }
    
    if (message == 10 ) {
       FLAG=1;
       Serial.println("BUZZER_ON! - code:10");
    } 
    
    if (message == 11 ) {
      FLAG=0;
      i=0;
      Serial.println("BUZZER_OFF! - code:11");    
    }
    
  if (millis() > time_now + period){
      time_now = millis();
      

      mySerial.print(volt1);
      mySerial.print(':');
      mySerial.print(volt2);
      mySerial.println('!');

     
      Serial.print(volt1);
      Serial.print(':');
      Serial.print(volt2);
      Serial.print("! ");
      Serial.println(FLAG);

    }
    delay(1);   

      if(i==203){i=1;}
      if(FLAG==1){
      Serial.println("gram");
      //for (int i=0;i<203;i++){              //203 is the total number of music notes in the song
      int wait = duration[i] * songspeed;
      tone(buzzer,notes[i],wait); 
      delay(wait);
      ++i;
    }

}
